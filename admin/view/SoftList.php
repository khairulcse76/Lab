﻿<?php 
include_once '../vendor/autoload.php';
use labApps\Lab\Software\Software;
$object=new Software();

$data=$object->ViewAllSoft();
include '../inc/header.php';


?>

<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="SoftAdd.php">New Software</a></li>
                <li class="ic-grid-tables"><a href="SoftList.php"><span></span>View All Instalation Softwer</a></li>
                <li class="ic-charts"><a href="#"><span>Visit Website</span></a></li>
            </ul>
 </div>
<?php
include '../inc/sidebar.php';
 
 
?>
 
        <div class="grid_10">
            <div class="box round first grid">
                <h2>SoftWare List
                
                    
                      <?php if(isset( $_SESSION['update_msg'])) {  ?>
                             
                    <span style="margin-left: 200px;"> <?php echo  $_SESSION['update_msg']; unset( $_SESSION['update_msg']); ?></span> 
                             
                             <?php } ?>  
                
                </h2>
                 
                                         
                <div class="block">        
                         <table class="data display datatable" id="example">
                            
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Software Name</th>
							<th>Software Version</th>
                                                        <th>Lab Name</th>
							<th>Software Type</th>
                                                        <th colspan="">Action</th>
						</tr>
					</thead>
					<tbody>
                                            <?php 
                                            $id=1;
                                            foreach ($data as $row) {
                                            ?>
                                                                                          
                                            <tr class="odd gradeX">
							<td><?php echo $id++; ?></td>
							<td><?php echo $row['software_title'] ?></td>
                                                        <td><?php echo $row['version'] ?></td>
                                                        <td><?php echo $row['labinfo_id'] ?></td>
                                                        <td><?php echo $row['software_type'] ?></td>
							<td><a href="SoftEdit.php?unique_id=<?php echo $row['unique_id']; ?>">Edit</a> ||
                                                            <a href="SoftDelete.php?unique_id=<?php echo $row['unique_id']; ?>">Delete</a></td>
                                            </tr>
                                                
                                            <?php } ?>
						
						
					</tbody>
                         </table>
               </div>
            </div>
        </div>
<script type="text/javascript">
	$(document).ready(function () {
	    setupLeftMenu();

	    $('.datatable').dataTable();
	    setSidebarHeight();
	});
</script>
<?php include '../inc/footer.php';?>

