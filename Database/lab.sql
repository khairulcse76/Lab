--
-- Database: `lab`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `title` varchar(111) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `course_type` varchar(50) NOT NULL,
  `course_fee` varchar(111) NOT NULL,
  `is_offer` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `unique_id`, `title`, `duration`, `description`, `course_type`, `course_fee`, `is_offer`, `created`, `updated`, `deleted`) VALUES
(1, 'ksjdhf8374lks', 'Web App development PHP', 'three month', 'three month duration application trining', 'premeum', '40000', 0, '2016-08-10 00:00:00', '2016-08-10 00:00:00', '2016-08-23 07:00:00'),
(2, 'dkfjd82347k', 'Dot Net', 'four month', 'alkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description dis', 'Free', '', 0, '2016-08-01 00:00:00', '2016-08-24 00:00:00', '2016-08-24 00:00:00'),
(3, 'ksdjf984sldkj', 'Java', 'two MOnth', 'alkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description dis', 'Free', '', 0, '2016-08-30 00:00:00', '2016-08-11 00:00:00', '2016-08-03 00:00:00'),
(4, '', 'HTML', 'One month', 'alkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description dis', 'Premeum', '4000', 0, '2016-08-16 00:00:00', '2016-08-09 00:00:00', '2016-08-17 00:00:00'),
(5, 'ksdjf984sldkj', 'Java', 'two MOnth', 'alkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description dis', 'Free', '', 0, '2016-08-30 00:00:00', '2016-08-11 00:00:00', '2016-08-03 00:00:00'),
(6, '', 'HTML', 'One month', 'alkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description dis', 'Premeum', '4000', 0, '2016-08-16 00:00:00', '2016-08-09 00:00:00', '2016-08-17 00:00:00'),
(7, 'ksdjf984sldkj', 'Java', 'two MOnth', 'alkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description dis', 'Free', '', 1, '2016-08-30 00:00:00', '2016-08-11 00:00:00', '2016-08-03 00:00:00'),
(8, '', 'HTML', 'One month', 'alkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description disalkdak na dlkahfka Description dis', 'Premeum', '4000', 1, '2016-08-16 00:00:00', '2016-08-09 00:00:00', '2016-08-17 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_trainer_lab_mapping`
--

CREATE TABLE `course_trainer_lab_mapping` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `course_id` int(111) NOT NULL,
  `batch_no` varchar(111) NOT NULL,
  `lead_trainer` varchar(111) NOT NULL,
  `asst_trainer` varchar(111) NOT NULL,
  `lab_asst` varchar(111) NOT NULL,
  `lab_id` varchar(111) NOT NULL,
  `start_date` varchar(111) NOT NULL,
  `ending_date` varchar(111) NOT NULL,
  `start_time` varchar(111) NOT NULL,
  `ending_time` varchar(111) NOT NULL,
  `day` varchar(111) NOT NULL,
  `is_running` int(11) NOT NULL,
  `assigned_by` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_trainer_lab_mapping`
--

INSERT INTO `course_trainer_lab_mapping` (`id`, `unique_id`, `course_id`, `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`, `updated`, `deleted`) VALUES
(34, '57b29bc16380c', 2, '101', 'Zadid Hassan', 'sumon Ahmed', 'Sirajul Amin', '101', '2016-08-10', '2016-08-02', '3:10', '5:15', 'Saturday Monday Wednesday', 1, 'khairul islam', '2016-08-16 10:51:13', '2016-08-16 10:57:06', '2016-08-16 10:51:13'),
(35, '57b29c42e5b60', 3, '102', 'Zadid Hassan', 'SAMSUL ISLAM', 'Sanjay shakhari', '102', '2016-08-08', '2016-08-10', '7:30', '3:30', 'Saturday Monday Wednesday', 0, 'khairul islam', '2016-08-16 10:53:22', '2016-08-16 11:24:01', '2016-08-16 10:53:22'),
(36, '57b29d0681676', 1, '103', 'Khairul Islam', 'Ratul', 'Jamali', '103', '2016-08-01', '2016-08-07', '1:15', '8:30', 'Saturday Monday Wednesday', 0, 'khairul islam', '2016-08-16 10:56:38', '2016-08-16 11:22:03', '2016-08-16 10:56:38'),
(37, '57b2c0090f708', 2, '109', 'Zadid Hassan', 'sumon Ahmed', 'Jamali', 'Java 405', '2016-08-10', '2016-08-17', '3:15', '8:15', 'Saturday Monday Wednesday', 0, 'khairul islam', '2016-08-16 01:26:01', '2016-08-17 01:41:06', '2016-08-16 01:26:01');

-- --------------------------------------------------------

--
-- Table structure for table `installed_softwares`
--

CREATE TABLE `installed_softwares` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `labinfo_id` int(111) NOT NULL,
  `software_title` varchar(111) NOT NULL,
  `version` varchar(111) NOT NULL,
  `software_type` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `installed_softwares`
--

INSERT INTO `installed_softwares` (`id`, `unique_id`, `labinfo_id`, `software_title`, `version`, `software_type`, `created`, `updated`, `deleted`) VALUES
(2, '57ab91731dfee', 1, 'Avro', '52', 'Free', '2016-08-11 02:41:23', '2016-08-11 02:41:23', '2016-08-11 02:41:23'),
(4, '57ab96950439e', 2, 'windows', '10', 'Premium', '2016-08-11 03:03:17', '2016-08-11 03:03:17', '2016-08-11 03:03:17'),
(6, '57ab96ede48d5', 1, 'Bijoy', '52', 'Free', '2016-08-11 03:04:45', '2016-08-11 03:04:45', '2016-08-11 03:04:45'),
(7, '57ab9cc5ddce8', 1, 'PHP Strom', '5.6', 'Premium', '2016-08-11 03:29:41', '2016-08-11 03:29:41', '2016-08-11 03:29:41'),
(8, '57af1a53dbe2c', 1, 'PHP Strom', '12.2', 'Free', '2016-08-13 07:02:11', '2016-08-13 07:02:11', '2016-08-13 07:02:11');

-- --------------------------------------------------------

--
-- Table structure for table `labinfo`
--

CREATE TABLE `labinfo` (
  `id` int(11) NOT NULL,
  `course_id` int(100) NOT NULL,
  `lab_no` varchar(111) NOT NULL,
  `seat_capacity` varchar(111) NOT NULL,
  `projector_resolution` varchar(111) NOT NULL,
  `ac_status` varchar(111) NOT NULL,
  `pc_configuration` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `trainer_pc_configuration` varchar(255) NOT NULL,
  `table_capacity` varchar(100) NOT NULL,
  `internet_speed` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labinfo`
--

INSERT INTO `labinfo` (`id`, `course_id`, `lab_no`, `seat_capacity`, `projector_resolution`, `ac_status`, `pc_configuration`, `os`, `trainer_pc_configuration`, `table_capacity`, `internet_speed`, `created`, `updated`, `deleted`) VALUES
(1, 2, 'PHP 302', '40', 'yes', 'on', 'yes', 'windows ', 'somthing', '15', '20kb', '2016-08-23 00:00:00', '2016-08-24 00:00:00', '2016-08-03 00:00:00'),
(2, 3, 'Java 405', '40', 'yes ON', 'yes', 'windows', 'windows', 'windows 10', '15', '50kb', '2016-08-22 00:00:00', '2016-08-23 00:00:00', '2016-08-16 00:00:00'),
(3, 4, 'DotNet 305', '40', 'yes', 'on', 'yes', 'windows ', 'somthing', '15', '20kb', '2016-08-23 00:00:00', '2016-08-24 00:00:00', '2016-08-03 00:00:00'),
(4, 3, '306 internate', '40', 'yes ON', 'yes', 'windows', 'windows', 'windows 10', '15', '50kb', '2016-08-22 00:00:00', '2016-08-23 00:00:00', '2016-08-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE `trainers` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `edu_status` varchar(255) NOT NULL,
  `team` varchar(111) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `trainer_status` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(111) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `web` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainers`
--

INSERT INTO `trainers` (`id`, `unique_id`, `full_name`, `edu_status`, `team`, `courses_id`, `trainer_status`, `image`, `phone`, `email`, `address`, `gender`, `web`, `created`, `updated`, `deleted`) VALUES
(1, 'dghdfg1f54fg4', 'Khairul Islam', 'BSC CSE Bangladesh University, BTEB', 'Khairul Islam', 0, 'Lead Trainer', 'image', '0178498', 'kaiurl@gmail.com', 'dhaka, mohammadpur dhaka', 'Male', 'www.maxbagworld.com', '2016-08-11 00:00:00', '2016-08-11 00:00:00', '2016-08-11 00:00:00'),
(4, 'ksjhfskj483732jk', 'Khairul Islam', 'BSe', 'zadid', 5, 'trainer', 'image', 'phone', 'address@gmail.com', 'nothing', 'male', 'www.maxbagworld.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '57ac92323922a', 'khairul', 'Khairul', 'Zadid', 2, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '57ac94cf3211d', 'Ratul', 'BBA', 'Khairul Islam', 2, 'Assistant Trainer', '', '30655', 'khai@gmail.com', 'Dhaka, Mohammadpur', 'Female', 'www.maxbagworld.com', '2016-08-11 09:07:59', '2016-08-11 09:07:59', '2016-08-11 09:07:59'),
(7, '57ac94cf427a6', 'Ratul', 'BBA', 'Zadid', 1, 'Assistant Trainer', '', '30655', 'khai@gmail.com', 'Dhaka, Mohammadpur', 'Female', 'www.maxbagworld.com', '2016-08-11 09:07:59', '2016-08-11 09:07:59', '2016-08-11 09:07:59'),
(8, '57ac9620d8ac8', 'Zadid Hassan', 'Bangla', 'Zadid', 2, 'Lead Trainer', '', '201545121312', 'dfasdf@sadfad', 'Dhaka, Mohammadpur', 'Female', 'www.maxbagworld.com', '2016-08-11 09:13:36', '2016-08-11 09:13:36', '2016-08-11 09:13:36'),
(9, '57ac973eab03d', 'Final Insert', 'Bangladesh University lab Assistant 2011 ', 'Zadid', 2, 'Assistant Trainer', '', '01738073362', 'sdfgwsdg@gmal.com', 'Dhaka, Mohammadpur DhakaMohammad pur chittagong 1200', 'Female', 'www.maxbagworld.com', '2016-08-11 09:18:22', '2016-08-11 09:18:22', '2016-08-11 09:18:22'),
(10, '57ac98830ac2f', 'Sumon Mahmud', 'akjdfnk Assistant Trainer 2013 ', 'for java teacher', 3, 'lab Assistant', '', '35465', 'jjj@gmao.com', 'alkdjajkfnqk j dfsndkaj Khulna 5485', 'Male', '', '2016-08-11 09:23:47', '2016-08-11 09:23:47', '2016-08-11 09:23:47'),
(11, '57ac98db4c6db', 'Sirajul Amin', 'lkjakjd Lead Trainer 2016 ', 'aoakjdfl', 1, 'lab Assistant', '', '35454', 'sklja@gmail.com', 'akldfhaidfakl afdhdfih aldfjuio  asijfauilf  Dhaka 5254', 'Male', 'kdklakd', '2016-08-11 09:25:15', '2016-08-11 09:25:15', '2016-08-11 09:25:15'),
(12, '57ac990ac67e5', 'Zubayer Ahmed', 'Bangladesh University Lead Trainer 2016 ', 'for java teacher', 3, 'Lead Trainer', '', '01774460981', 'sdfgwsdg@gmal.com', 'Dhaka, Mohammadpur DhakaMohammad pur Rajshahi s', 'Male', 'uyguy', '2016-08-11 09:26:02', '2016-08-11 09:26:02', '2016-08-11 09:26:02'),
(13, '57ac997f0f875', 'khairul ', 'Bangladesh University Lead Trainer 2016 ', 'kldfjaskdj', 2, 'Lead Trainer', '', '01774460981', 'khairulislamtpi76@gmail.com', '  Dhaka ', 'Male', 'fgh', '2016-08-11 09:27:59', '2016-08-11 09:27:59', '2016-08-11 09:27:59'),
(14, '57b066926f310', 'SAMSUL ISLAM', 'Northan lab Assistant 2013 ', 'Zadid Alrushdi', 3, 'Assistant Trainer', '', '12356456', 'khairulislamtpi76@gmail.com', 'bangladesh bangladesh chittagong Faridpur', 'Male', '', '2016-08-14 06:39:46', '2016-08-14 06:39:46', '2016-08-14 06:39:46'),
(15, '57b066d177ca3', 'sumon  Ahmed', 'Jonaki Assistant Trainer 2014 ', 'Zadid Alrushdi', 2, 'Assistant Trainer', '', '01289856', 'sumon@gmail.com', 'bangladesh bangladesh chittagong Faridpur', 'Male', '', '2016-08-14 06:40:49', '2016-08-14 06:40:49', '2016-08-14 06:40:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(10) NOT NULL,
  `is_admin` int(10) NOT NULL,
  `is_delete` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  `Logintime` varchar(111) NOT NULL,
  `Logouttime` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `full_name`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `is_delete`, `created`, `updated`, `deleted`, `Logintime`, `Logouttime`) VALUES
(5, '57ab444e916b7', 'khairul islam', 'khairul', 'khairulislamtpi76@gmail.com', 'khairul', '', 0, 0, 0, '2016-08-10 09:12:14', '2016-08-10 09:12:14', '2016-08-10 09:12:14', '2016-08-17 01:07:17PM', '2016-08-17 01:07:13PM'),
(6, 'askfkaiqma', '', 'khairul', 'email@gamil.com', 'khairul', 'iamge', 0, 0, 0, '2016-08-10 09:12:14', '2016-08-10 09:12:14', '2016-08-10 09:12:14', '', '0'),
(7, 'fgdffdfs61dfsf', '', 'khairulislam', 'khairul@gmail.com', 'khairul', 'image', 0, 0, 0, '2016-08-14 00:00:00', '2016-08-14 00:00:00', '2016-08-14 00:00:00', '', '0'),
(8, '57b013d61ceda', '', 'khairul545', 'admin@gmial.com', 'aaaaaa', '', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0'),
(9, '57b01471cce4d', '', 'sharmin', 'shohag@gmail.com', 'sharmin', '', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labinfo`
--
ALTER TABLE `labinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `labinfo`
--
ALTER TABLE `labinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
