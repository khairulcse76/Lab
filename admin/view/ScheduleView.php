<?php 
include_once '../vendor/autoload.php';
use labApps\Lab\Schedule\Schedule;
$viewSchedule=new Schedule();
$data=$viewSchedule->prepare($_GET)->Scheduleshow();


include '../inc/header.php';
?> 
<style>
table, th {
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}
input[type=button], input[type=submit], input[type=button] {
    width: 100px;
    height: 40px;
    background-color: #cccccc;
}
input[type=button]:hover, input[type=submit]:hover, input[type=button]:hover {
    color: #ffffff;
    background-color: #666666;
}

</style>
<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="ScheduleAdd.php">New Training Schedule</a></li>
                <li class="ic-grid-tables"><a href="Overview.php"><span></span>Overview Training Schedule</a></li>
                <li class="ic-charts"><a href="http://www.bitm.org.bd/"><span>Visit Website</span></a></li>
            </ul>
 </div>
<?php 
include '../inc/sidebar.php';
 
 
?>
 <div class="grid_10">
            <div class="box round first grid">
                <h2>Training Schedule Details
                
                    <span style="float: right; margin-right: 50px; ">
                        <?php if($data['is_running']==0){
                            echo 'Course Disable...!';
                        }  else {
                            echo "Course Running...";
                        } ?>
                        
                    </span>
                      <?php if(isset( $_SESSION['error_msg'])) {  ?>
                             
                    <span style="margin-left: 200px;"> <?php echo  $_SESSION['error_msg']; unset( $_SESSION['error_msg']); ?></span> 
                             
                             <?php } ?>  
                
                </h2>
                <form action="ScheduleStore.php" method="Post">

                    <table>
                      <tr>
                          <td><h2>Trainer</h2> 
                            <table>
                                <tr>
                                    <td><label>Lead Trainer</label>:</td>
                                    <td><?php echo $data['lead_trainer']; ?></td>
                                </tr>
                                <tr>
                                    <td><label>Assistant Trainer</label>:</td>
                                    <td><?php echo $data['asst_trainer']; ?></td>
                                </tr>
                                <tr>
                                    <td><label>Assign by</label>:</td>
                                    <td><?php echo $data['assigned_by']; ?></td>
                                </tr>
                                
                            </table>
                        </td>
                        <td><h2>Course Info</h2>
                            <table>
                                <tr>
                                    <td><label>Course Title</label>:</td>
                                    <td><?php echo $data['title']; ?></td>
                                </tr>
                                <tr>
                                    <td><label>Batch NO</label>:</td>
                                    <td><?php echo $data['batch_no']; ?></td>
                                </tr>
                                <tr>
                                    <td><label>Lab No</label>:</td>
                                    <td><?php echo $data['lab_id']; ?></td>
                                </tr>
                                <tr>
                                    <td><label>Lab Assistant</label>:</td>
                                    <td><?php echo $data['lab_asst']; ?></td>
                                </tr>
                            </table>
                            
                        </td>
                        
                        <td> <h2>Duration</h2>
                            <table>
                                <tr>
                                    <td><label>Start date</label>:</td>
                                    <td><?php echo $data['start_date']; ?></td>
                                </tr>
                                <tr>
                                    <td><label>End date</label>:</td>
                                    <td><?php echo $data['ending_date']; ?></td>
                                </tr>
                                <tr>
                                    <td><label>Start time</label>:</td>
                                    <td><?php echo $data['start_time']; ?></td>
                                </tr>
                                <tr>
                                    <td><label>End time</label>:</td>
                                    <td><?php echo $data['ending_time']; ?></td>
                                </tr>
                                <tr>
                                    <td><label>Days per Week</label>:</td>
                                    <td><?php echo $data['day']; ?></td>
                                </tr>
                            </table></td>
                     <!--??/--<td colspan="3"><input type="submit" value="Submit"/><input type="reset" value="Reset"/></td>-->
                      <tr>
                          <td colspan="3">
                              <h2 style="background:none; ">
                                  <span style="float: right; margin-right: 50px;"><input type="button" value="Edit" onclick="window.location.href='./ScheduleEdit.php?unique_id=<?php echo $data['unique_id']; ?>'"></span>
                                  <span style="float: right; margin-right: 50px;"><input type="button" value="Delete" onclick="window.location.href='./ScheduleDelete.php?unique_id=<?php echo $data['unique_id']; ?>'"></span>
                                  <span style="float: right; margin-right: 50px;"><input type="button" value="Back" onclick="window.location.href='./Overview.php'"></span>
                                  
                                  
                              </h2>
                          </td>
                      </tr>
                      </tr>
                      
                      
                    </table>

                </form>
            </div>
                <?php include '../inc/footer.php';?>
                


                