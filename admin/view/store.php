<?php
include_once '../vendor/autoload.php';

use labApps\Lab\Registration\user;

$object=new user();

$_SESSION['AllDAta']=$_POST;
//print_r($_POST);
$username=$_POST['username'];
$password=$_POST['password'];
$confirmPass=$_POST['confmpass'];
$email=$_POST['email'];
if(empty($username) || empty($password) || empty($confirmPass) || empty($email) )
{
     $_SESSION['error_msg']= 'Field must be not empty';
     header('location:Register.php'); 
} else {
    if(strlen($username)<5 || strlen($username)>10)
    {
       $_SESSION['error_msg']= 'User name must 5 to 10 character';
         header('location:Register.php'); 
    }  else {
       if(strlen($password)<6 || strlen($password)>12)
       {
           $_SESSION['error_msg']= 'Password must 6 to 12 character';
             header('location:Register.php');
       }  else {
           if($password!=$confirmPass)
           {
               $_SESSION['error_msg']= 'Password and confirmation password dose not match.';
                header('location:Register.php');
           }  else {
               if (check_email_address($email)) {
                 $object->prepare($_POST)->store();
                } else {
                    $_SESSION['error_email'] = 'Invalid email format';
                     header('location:Register.php');
                }
           }
       }
    }
    
}
 function check_email_address($email) {
    // First, we check that there's one @ symbol, and that the lengths are right
    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
        // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
        return false;
    }
    // Split it into sections to make life easier
    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++) {
         if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
            return false;
        }
    }    
    if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
        $domain_array = explode(".", $email_array[1]);
        if (sizeof($domain_array) < 2) {
                return false; // Not enough parts to domain
        }
        for ($i = 0; $i < sizeof($domain_array); $i++) {
            if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
                return false;
            }
        }
    }
    return true;
}
//$object->prepare($_POST)->store();
//echo '<pre>';
//print_r($_POST);