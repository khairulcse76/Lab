<?php

namespace labApps\Lab\Schedule;

use PDO;

class Schedule {

    public $id = '';
    public $unique_id = '';
    public $course_id = '';
    public $batch_no = '';
    public $lead_trainer = '';
    public $user = 'root';
    public $asst_trainer = '';
    public $lab_asst = '';
    public $lab_id = '';
    public $start_date = '';
    public $ending_date = '';
    public $start_time = '';
    public $ending_time = '';
    public $day = '';
    public $is_running = '';
    public $assigned_by = '';
    public $presentAddress = '';
    public $permanentAddress = '';
    public $zipcode = '';
    public $City = '';
    public $gender = '';
    public $web = '';
    public $lastDegree = '';
    public $pass = '';
    public $connection = '';

    public function __construct() {
//        session_start();
//       date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=lab', $this->user, $this->pass);
        echo "database pai";
    }

    public function prepare($data = '') {
        if (array_key_exists('courseinfo', $data)) {
            $this->course_id = $data['courseinfo'];
        }
        if (array_key_exists('batchno', $data)) {
            $this->batch_no = $data['batchno'];
        }
        if (array_key_exists('labname', $data)) {
            $this->lab_id = $data['labname'];
        }

        if (array_key_exists('ledtrainer', $data)) {
            $this->lead_trainer = $data['ledtrainer'];
        }
        if (array_key_exists('unique_id', $data)) {
            $this->unique_id = $data['unique_id'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('asstrainer', $data)) {
            $this->asst_trainer = $data['asstrainer'];
        }
        if (array_key_exists('labass', $data)) {
            $this->lab_asst = $data['labass'];
        }
        if (array_key_exists('date1', $data)) {
            $this->start_date = $data['date1'];
        }
        if (array_key_exists('date2', $data)) {
            $this->ending_date = $data['date2'];
        }
        if (array_key_exists('slt_days', $data)) {
            $this->day = $data['slt_days'];
        }
        if (array_key_exists('strttime', $data)) {
            $this->start_time = $data['strttime'];
        }
        if (array_key_exists('endTime', $data)) {
            $this->ending_time = $data['endTime'];
        }
//        if(array_key_exists('trainerstatus', $data))
//       {
//           $this->trainer_status=$data['trainerstatus'];
//       }
//        if(array_key_exists('team', $data))
//       {
//           $this->team=$data['team'];
//       }
//        if(array_key_exists('gender', $data))
//       {
//           $this->gender=$data['gender'];
//       }
//        if(array_key_exists('phone', $data))
//       {
//           $this->phone=$data['phone'];
//       }
//        if(array_key_exists('web', $data))
//       {
//           $this->web=$data['web'];
//       }
//        if(array_key_exists('CourseId', $data))
//       {
//           $this->courses_id=$data['CourseId'];
//       }
//        if(array_key_exists('image', $data))
//       {
//           $this->image=$data['image'];
//       }
        return $this;
    }

    public function management_store() {

        try {

            $query = "INSERT INTO `course_trainer_lab_mapping`(id, course_id, batch_no, lead_trainer, asst_trainer, lab_asst,
                 lab_id, start_date, ending_date, start_time, ending_time, day, is_running)
                 VALUES(:id, :cri, :bno, :ltr, :ast, :las, :lid,:sdate,:edate, :stime, :etime,
                 :d,:isrun)";
           
            $stmt = $this->connection->prepare($query);
            
           $stmt->execute(array(
                ':id' => NULL,
                ':cri' => $this->course_id,
                ':bno' => $this->batch_no,
                ':ltr' => $this->lead_trainer,
                ':ast' => $this->asst_trainer,
                ':las' => $this->lab_asst,
                ':lid' => $this->lab_id,
                ':sdate' => $this->start_date,
                ':edate' => $this->ending_date,
                ':stime' => $this->start_time,
                ':etime' => $this->ending_time,
                ':d' => $this->day,
                ':isrun' =>1
//                ':created' => date("Y-m-d h:i:sA"),
            ));
            $count = $stmt->rowCount();
            echo "sucessfull $count";
//            if ($result) {
//                $_SESSION['error_msg'] = '<b style="color: blue;">Information Inserted</b>';
//                unset($_SESSION['AllDAta']);
//                header('location:TrainerAdd.php');
//            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    public function ViewAllSchedule()
    {
//          trainers.trainer_status
           $query="SELECT course_trainer_lab_mapping.*, courses.title, labinfo.lab_no
           FROM course_trainer_lab_mapping
           INNER JOIN courses
           ON courses.id = course_trainer_lab_mapping.course_id
           INNER JOIN labinfo
           ON labinfo.id = course_trainer_lab_mapping.lab_id
           ORDER BY course_trainer_lab_mapping.id DESC";

            $stmt=  $this->connection->query($query);
             $stmt->execute();
             $table=$stmt->fetchAll();
             return $table;
       
    
    }

}
