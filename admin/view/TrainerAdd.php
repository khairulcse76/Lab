﻿<?php 
session_start();
include '../inc/header.php'; 

?> 

<div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="dashboard.php"><span>Dashboard</span></a> </li>
                <li class="ic-typography"><a href="TrainerAdd.php">Add Trainer</a></li>
                                <li class="ic-grid-tables"><a href="TrainerList.php"><span></span>View All Trainer</a></li>
                <li class="ic-charts"><a href="#"><span>Visit Website</span></a></li>
            </ul>
 </div>
<?php 
include '../inc/sidebar.php';
 
 
?>

<div class="grid_10">
            <div class="box round first grid">
                <h2>New Trainer</h2>
               <div class="block copyblock"> 
                    <?php if(isset($_SESSION['error_msg'])) { ?>
                       <p><?php echo $_SESSION['error_msg']; unset($_SESSION['error_msg']); ?></p>
                         <?php } ?>
                   <form action="TrainerStore.php" method="POST">
                      
                    <table class="form">
                       
                        <tr>
                            <td>
                                <table class="form">
                                    <tr>
                                        <td colspan="2"><u><h2>Personal Information</h2></u></td>
                                    </tr>
                                    <tr>
                                        <td>First Name<span style="color: red;">*</span> <br><input type="text" name="firstName"
                                                                                                    value="<?php if(isset($_SESSION['AllDAta']['firstName'])){ echo $_SESSION['AllDAta']['firstName']; unset($_SESSION['AllDAta']['firstName']); } ?>" placeholder="Enter First Name..." class="medium" /> <?php if(isset($_SESSION['fulN_emt_msg'])) { echo $_SESSION['fulN_emt_msg']; unset($_SESSION['fulN_emt_msg']); } ?></td>
                                        <td>Last Name<span style="color: red;"></span> <br><input type="text" name="lastName" value="<?php if(isset($_SESSION['AllDAta']['lastName'])){ echo $_SESSION['AllDAta']['lastName']; unset($_SESSION['AllDAta']['lastName']); } ?>" placeholder="Enter Last Name..." class="medium" /> <?php if(isset($_SESSION['fulN_emt_msg'])) { echo $_SESSION['fulN_emt_msg']; unset($_SESSION['fulN_emt_msg']); } ?></td>
                                     </tr>
                                      <tr>
                                        <td>Phone<span style="color: red;">*</span><br><input type="number" name="phone" value="<?php if(isset($_SESSION['AllDAta']['phone'])){ echo $_SESSION['AllDAta']['phone']; unset($_SESSION['AllDAta']['phone']); } ?>" placeholder="Phone " class="medium" /><?php if(isset($_SESSION['E_emt_msg'])) { echo $_SESSION['E_emt_msg']; unset($_SESSION['E_emt_msg']); } ?></td>
                                        <td>E-mail<span style="color: red;"></span> <br><input type="email" name="email" placeholder="Example@gmail.com..." value="<?php if(isset($_SESSION['AllDAta']['email'])){ echo $_SESSION['AllDAta']['email']; unset($_SESSION['AllDAta']['email']); } ?>" class="medium" /><?php if(isset($_SESSION['p_emt_msg'])) { echo $_SESSION['p_emt_msg']; unset($_SESSION['p_emt_msg']); } ?></td>
                                   </tr>
                                   
                                   <tr>
                                       <td colspan="2" style="float: left; margin-left: 100px;">Gender<span style="color: red;">*</span>
                                     <input type="radio" name="gender" value="Male" checked> Male
                                    <input type="radio" name="gender" value="Female"> Female<br>
                                     </td>
                                 </tr> 
                                </table>
                            </td>
                            
                        </tr>
                         <tr>
                             <td>
                                 <table class="form">
                                     <tr>
                                         <td colspan="2"><br><u><h2>Educational  status</h2></u><span style="color: red;"></span>
                                                 
                                     </tr>
                                     <tr>
                                         <td width="326">University<span style="color: red;"></span><br><input type="text" name="University" value="<?php if(isset($_SESSION['AllDAta']['University'])){ echo $_SESSION['AllDAta']['University']; unset($_SESSION['AllDAta']['University']); } ?>" placeholder="University..." class="medium" /><?php if(isset($_SESSION['E_emt_msg'])) { echo $_SESSION['E_emt_msg']; unset($_SESSION['E_emt_msg']); } ?></td>
                                  
                                 <td>Subject<span style="color: red;"></span><br><input type="text" name="subject" value="<?php if(isset($_SESSION['AllDAta']['subject'])){ echo $_SESSION['AllDAta']['subject']; unset($_SESSION['AllDAta']['subject']); } ?>" placeholder="Subject" class="medium" /><?php if(isset($_SESSION['E_emt_msg'])) { echo $_SESSION['E_emt_msg']; unset($_SESSION['E_emt_msg']); } ?></td>
                        </tr>
                                          <tr>
                                              <td width="254">Last Degree <span style="color: red;">*</span><br>
                                      <select name="lastDegree">
                                    <option value="Lead Trainer">PHD</option>
                                    <option value="Assistant Trainer">MSc</option>
                                    <option value="lab Assistant">MBA</option>
                                  </select>
                                 </td>
                                         <td>Passing Year<span style="color: red;">*</span><br>
                                             <select name="passingYear">
                                     
                                    <option value="2016">2016</option>
                                    <option value="2015">2015</option>
                                    <option value="2014">2014</option>
                                    <option value="2013">2013</option>
                                    <option value="2012">2012</option>
                                    <option value="2011">2011</option>
                                    <option value="2010">2010</option>
                                    <option value="2009">2009</option>
                                    <option value="2008">2008</option>
                                    <option value="2007">2007</option>
                                    </select>
                             </td>
                             
                              
                          </tr></table><hr>
                          
                          <table class="form">
                                     <tr>
                                         <td colspan="2"><br><u><h2>ADDRESS</h2></u><span style="color: red;"></span>
                                                 
                                     </tr>
                                     <tr>
                                  <td width="254">Present Address <span style="color: red;"></span><br>
                                      <input type="text" name="present_address" style="width: 250px; height: 80px;" value="<?php if(isset($_SESSION['AllDAta']['present_address'])){ echo $_SESSION['AllDAta']['present_address']; unset($_SESSION['AllDAta']['present_address']); } ?>" class="medium" /><?php if(isset($_SESSION['E_emt_msg'])) { echo $_SESSION['E_emt_msg']; unset($_SESSION['E_emt_msg']); } ?></td>
                                  <td width="326">Permanent Address<span style="color: red;"></span><br>
                                      <input type="text" name="permanent_address" style="width: 250px; height: 80px;" value="<?php if(isset($_SESSION['AllDAta']['permanent_address'])){ echo $_SESSION['AllDAta']['permanent_address']; unset($_SESSION['AllDAta']['permanent_address']); } ?>"  class="medium" /><?php if(isset($_SESSION['E_emt_msg'])) { echo $_SESSION['E_emt_msg']; unset($_SESSION['E_emt_msg']); } ?></td>
                        </tr>
                                          <tr>
                                         <td>City<span style="color: red;"></span><br>
                                             <select name="city">
                                     
                                    <option value="Dhaka">Dhaka</option>
                                    <option value="chittagong">Chittagong</option>
                                    <option value="Khulna">Khulna</option>
                                    <option value="Rajshahi">Rajshahi</option>
                                    <option value="Rangpur">Rangpur</option>
                                    <option value="Bogra">Bogra</option>
                                    <option value="Sylet">Sylet</option>
                                    </select>
                             </td>
                              <td>Zip<span style="color: red;"></span><br><input type="text" name="zip" value="<?php if(isset($_SESSION['AllDAta']['zip'])){ echo $_SESSION['AllDAta']['zip']; unset($_SESSION['AllDAta']['zip']); } ?>" placeholder="Subject" class="medium" /><?php if(isset($_SESSION['E_emt_msg'])) { echo $_SESSION['E_emt_msg']; unset($_SESSION['E_emt_msg']); } ?></td>
                          </tr></table><hr>
                          </td>  
                         </tr>
                         <tr>
                             <td>Team<span style="color: red;">*</span><br><input type="text" name="team" value="<?php if(isset($_SESSION['AllDAta']['team'])){ echo $_SESSION['AllDAta']['team']; unset($_SESSION['AllDAta']['team']); } ?>" placeholder="Team" class="medium" /><?php if(isset($_SESSION['E_emt_msg'])) { echo $_SESSION['E_emt_msg']; unset($_SESSION['E_emt_msg']); } ?></td>
                        </tr>
                        <tr>
                             <td>Trainer Status <span style="color: red;">*</span><br>
                                 <select name="trainerstatus">
                                    <option value="Lead Trainer">Lead Trainer</option>
                                    <option value="Assistant Trainer">Assistant Trainer</option>
                                    <option value="lab Assistant">lab Assistant</option>
                                  </select>
                             </td>
                        </tr>

                        <tr>
                             <td>Profile Picture <span style="color: red;"></span> <br><input type="file" name="image" value="<?php if(isset($_SESSION['AllDAta']['image'])){ echo $_SESSION['AllDAta']['image']; unset($_SESSION['AllDAta']['image']); } ?>" class="medium" /><?php if(isset($_SESSION['im_emt_msg'])) { echo $_SESSION['im_emt_msg']; unset($_SESSION['im_emt_msg']); } ?></td>
                        </tr>
                         <tr>
                             <td>Web<span style="color:#999;"> [Optional] </span> <br><input type="web" name="web" placeholder="Web Address" value="<?php if(isset($_SESSION['AllDAta']['web'])){ echo $_SESSION['AllDAta']['web']; unset($_SESSION['AllDAta']['web']); } ?>" class="medium" /><?php if(isset($_SESSION['p_emt_msg'])) { echo $_SESSION['p_emt_msg']; unset($_SESSION['p_emt_msg']); } ?></td>
                        </tr>
                         <tr>
                             <td>Course Name<span style="color:#999;">*</span> <br>
                             <select name="CourseId">
                                    <option value="1">Php</option>
                                    <option value="2">Java</option>
                                    <option value="3">Dot Net</option>
                                  </select>
                             </td>
                        </tr>
			<tr> 
                            <td>
                                <input type="submit" Value="Insert" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>

<?php include '../inc/footer.php';?>