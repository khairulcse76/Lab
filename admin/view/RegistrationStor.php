<?php
include_once '../vendor/autoload.php';

use labApps\Lab\Registration\Registration;

$RegisterObj= new Registration();

$username=$_POST['username'];
$password=$_POST['password'];
$confirmPass=$_POST['confmpass'];
$email=$_POST['email'];

if(empty($username) || empty($password) || empty($confirmPass) || empty($email) )
{
     $_SESSION['error_msg']= 'Field must be not empty';
     header('location:Register.php'); 
} else {
    if(strlen($username)<5 || strlen($username)>12)
    {
       $_SESSION['error_msg']= 'User name must 5 to 12 character';
         header('location:Register.php'); 
    }  else {
       if(strlen($password)<6 || strlen($password)>15)
       {
           $_SESSION['error_msg']= 'Password must 6 to 15 character';
             header('location:Register.php');
       }  else {
           if($password!=$confirmPass)
           {
               $_SESSION['error_msg']= 'Password and confirmation password dose not match.';
                header('location:Register.php');
           }  else {
              $RegisterObj->prepare($_POST)->Store();
              
           }
       }
    }
}
//$object->prepare($_POST)->store();
//echo '<pre>';
//print_r($_POST);