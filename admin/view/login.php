<?php

include_once '../vendor/autoload.php';

use labApps\Lab\User\users;

$object=new users();

    if(isset($_SESSION['user']))
{
    $_SESSION['login_msg']='<span style="color: red; font-size:16px; ">please logout first</span>';
     header('location:dashboard.php');
} else{
  if($_SERVER['REQUEST_METHOD']=='POST')
  {
   $username=$_POST['username'];
   $password=$_POST['password'];
   
   if(empty($username) || empty($password))
   {
       $_SESSION['err_msg']= "Field must be not empty..!";
   }  else {
       $loginchk=$object->prepare($_POST)->login();
       
       if($loginchk)
       {
           $_SESSION['login_msg']='<b style=" color:blue; font-size: 16px; ">Login Successfull';
           header('location:dashboard.php');
       }  else {
       
           $_SESSION['err_msg']='<b style=" color:blue; font-size: 16px; ">user name or password dose not match..!!';
       }
    }
  }
}
?>


<head>
<meta charset="utf-8">
<title>Login</title>
    <link rel="stylesheet" type="text/css" href="stylelogin.css" media="screen" />
</head>
<body>
<div class="container">
	<section id="content">
		<form action="
                      " method="post">
			<h1>Admin Login</h1>
                       <?php if(isset($_SESSION['err_msg'])) { ?>
                        <p style="color:red;"><?php echo $_SESSION['err_msg']; unset($_SESSION['err_msg']); ?></p>
                         <?php } ?>
			<div>
				<input type="text" placeholder="Username"  name="username"/>
			</div>
			<div>
				<input type="password" placeholder="Password"  name="password"/>
			</div>
			<div>
                            <input type="submit" value="Log in" /><a href="Register.php">Sing up</a>
			</div>
		</form><!-- form -->
		<div class="button">
			<a href="#">BACKS\ASH</a>
		</div>
	</section>
</div>
</body>
</body>
</html>
