<?php
session_start();


?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration Form</title>
        
        <style>
            body{
                background: #DCDDDF;
            }
            *{
    margin: 0;
    padding: 0;
    outline: 0;
}
    .wrapper{
        width: 900px;
        margin: 0 auto;
    }
   
    .content{
        min-height: 600px;
        overflow: hidden;
        width: 100%;
    }
     h3{
        line-height: 50px;
        margin: 0 auto;
        margin-bottom: 20px;
        text-align: center;
        color: #666666;
        text-decoration:underline solid #000033;
    }
    .msg{
        color: #0000ff;
        margin: 0 auto;
        text-align: center;
        font-size: 16px;
        font-weight: bold;
        text-transform: capitalize;
    }
    .login_reg{
        background: #F9F9F9 none repeat scroll 0 0;
        margin: 0 auto;
        margin-top: 50px;
        border:1px solid #999999;
        width: 500px;
        padding: 20px;

    }
    input[type="text"],input[type="email"],input[type="password"]{
        border: 1px solid #d5d5d5;
        margin-bottom: 6px;
        margin-left: 10px;
        width: 285px;
        padding: 5px;
    }
    input[type="submit"],input[type="reset"],input[type="button"]{
    background: #888; 
    border: 1px solid #626262;
    border-radius: 3px;
    cursor: pointer;
    color: #fff;
    font-weight: bold;
    padding: 5px 15px;
    float: right;
    margin-right: 20px;
    }
    input[type="submit"]:hover,input[type="reset"]:hover{
        background: #666;
    }
    table{
        margin: 0 auto;
    }
   
    .footer{
        background: #cccccc none repeat scroll 0 0;
        padding: 20px;
        color: #fff;
        text-align: center;
    }
        </style>
    </head>
    <body>
          <div class="wrapper">
            <div class="content">
                <div class="login_reg">
                    <form action="store.php" method="POST">
                        <table>
                            <tr>
                                <th colspan=""> <h3>BACKS<b>\</b>ASH REGISTRATION FORM</h3></th>
                            </tr>
                             <?php 
                                if(isset($_SESSION['error_msg']))
                                { ?>
                                <tr><td style="color: red; text-align:  center; font-size: 16px;"><?php

                                echo $_SESSION['error_msg'];
                                unset($_SESSION['error_msg']);
                                ?></td></tr>
                                <?php }

                                ?>
                            <tr>
                                <td>Full Name<br>
                                    <input type="text" name="fullname" placeholder="Please give your Full Name" 
                                     value="<?php if(isset($_SESSION['AllDAta']['fullname'])){ echo $_SESSION['AllDAta']['fullname']; unset($_SESSION['AllDAta']['fullname']); } ?>"></td>
                            </tr>
                            <tr>
                                <td>User Name <span style="color:red">*</span><br>
                                    <input type="text" name="username" placeholder="Please give your username" 
                                           value="<?php if(isset($_SESSION['AllDAta']['username'])){ echo $_SESSION['AllDAta']['username']; unset($_SESSION['AllDAta']['username']); } ?>"></td>
                            </tr>
                            
                             <tr>
                                <td>Password <span style="color:red">*</span><br>
                                    <input type="password" name="password" placeholder="Please give your password" 
                                           value="<?php if(isset($_SESSION['AllDAta']['password'])){ echo $_SESSION['AllDAta']['password']; unset($_SESSION['AllDAta']['password']); } ?>"></td>
                            </tr>
                           
                             <tr>
                                <td>Confirm Password <span style="color:red">*</span><br>
                                    <input type="password" name="confmpass" placeholder="Reatype your password"></td>
                            </tr>
                            
                              <tr>
                                <td>Email<span style="color:red">*</span><br>
                                    <input type="text" name="email" placeholder="Please give your Email address" 
                                           value="<?php if(isset($_SESSION['AllDAta']['email'])){ echo $_SESSION['AllDAta']['email']; 
                                               unset($_SESSION['AllDAta']['email']); } ?>"><br/>
                                    
                                     <?php 
                                        if(isset($_SESSION['error_email']))
                                        { ?>
                                        <p style="color: red; text-align:  center; font-size: 16px;"><?php

                                        echo $_SESSION['error_email'];
                                        unset($_SESSION['error_email']);
                                        ?></p>
                                        <?php }
                                ?>
                                    
                                </td>
                            </tr>
                             <tr>
                                 <td  colspan=""><br><input type="submit" value="Register">
                                     <input type="reset" value="Reset">
                                     <input type="button" value="Login" onclick="window.location.href='./login.php'">
                                 </td>
                            </tr>

                        </table>
                    </form>
                </div>
            </div>
            
            </div>
    </body>
</html>
