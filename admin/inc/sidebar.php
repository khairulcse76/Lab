<div class="grid_2">
    <div class="box sidemenu">
        <div class="block" id="section-menu">
            <ul class="section menu">
                <li><a href="#">Management</a></li>
               <li><a class="menuitem">Training Management</a>
                   <ul class="submenu">
                        <li><a href="ScheduleAdd.php">Insert New Training Schedule</a></li>
                        <li><a href="Overview.php">Overview</a></li>
                    </ul>
                   
                </li>
				
                 <li><a class="menuitem">Users Management</a>
                    <ul class="submenu">
                        <li><a href="UserAdd.php">Add user</a></li>
                        <li><a href="Userlist.php">User list</a></li>
                    </ul>
                </li>
				<li><a class="menuitem">Trainer</a>
                    <ul class="submenu">
                        <li><a href="TrainerAdd.php">Add Trainer</a> </li>
                        <li><a href="TrainerList.php">Trainer List</a> </li>
                    </ul>
                </li>
                <li><a class="menuitem">Course</a>
                    <ul class="submenu">
                        <li><a href="catadd.php">Add Course info</a> </li>
                        <li><a href="catlist.php">Course info List</a> </li>
                    </ul>
                </li>
                <li><a class="menuitem">Lab</a>
                    <ul class="submenu">
                        <li><a href="productadd.php">Add Lab info</a> </li>
                        <li><a href="productlist.php">Lab info List</a> </li>
                    </ul>
                </li>
                
                <li><a class="menuitem">Software</a>
                    <ul class="submenu">
                        <li><a href="SoftAdd.php">Add Software</a> </li>
                        <li><a href="SoftList.php">Software List</a> </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>