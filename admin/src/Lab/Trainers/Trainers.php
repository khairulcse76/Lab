<?php
namespace labApps\Lab\Trainers;
use PDO;
class Trainers {

    public $id='';
    public $unique_id='';
    public $first_name='';
    public $last_name='';
    public $email='';
    public $user='root';
    public $university='';
    public $passingyear='';
    public $eduTitle='';
    public $eduSub='';
    public $eduBoard='';
    public $team='';
    public $courses_id='';
    public $trainer_status='';
    public $image='';
    public $phone='';
    public $presentAddress='';
    public $permanentAddress='';
    public $zipcode='';
    public $City='';
    public $gender='';
    public $web='';
    public $lastDegree='';
    public $pass='';
    public $connection='';




    public function __construct() {
        session_start();
       date_default_timezone_set("Asia/Dhaka");
        $this->connection = new PDO('mysql:host=localhost;dbname=lab', $this->user, $this->pass);
        
    }
    
    

    public function prepare($data='')
    {
       if(array_key_exists('firstName', $data))
       {
           $this->first_name = $data['firstName'];
       }
       if(array_key_exists('lastName', $data))
       {
           $this->last_name=$data['lastName'];
       }
       if(array_key_exists('email', $data))
       {
           $this->email=$data['email'];
       }
       
        if(array_key_exists('lastDegree', $data))
       {
           $this->lastDegree=$data['lastDegree'];
       }
        if(array_key_exists('unique_id', $data))
       {
           $this->unique_id=$data['unique_id'];
       }
        if(array_key_exists('id', $data))
       {
           $this->id=$data['id'];
       }
       if(array_key_exists('University', $data))
       {
           $this->university=$data['University'];
       }
        if(array_key_exists('passingYear', $data))
       {
           $this->passingyear=$data['passingYear'];
       }
       if(array_key_exists('subject', $data))
       {
           $this->eduSub=$data['subject'];
       }
        if(array_key_exists('present_address', $data))
       {
           $this->presentAddress=$data['present_address'];
       }
       if(array_key_exists('permanent_address', $data))
       {
           $this->permanentAddress=$data['permanent_address'];
       }
        if(array_key_exists('city', $data))
       {
           $this->City=$data['city'];
       }
       if(array_key_exists('zip', $data))
       {
           $this->zipcode=$data['zip'];
       }
        if(array_key_exists('trainerstatus', $data))
       {
           $this->trainer_status=$data['trainerstatus'];
       }
        if(array_key_exists('team', $data))
       {
           $this->team=$data['team'];
       }
        if(array_key_exists('gender', $data))
       {
           $this->gender=$data['gender'];
       }
        if(array_key_exists('phone', $data))
       {
           $this->phone=$data['phone'];
       }
        if(array_key_exists('web', $data))
       {
           $this->web=$data['web'];
       }
        if(array_key_exists('CourseId', $data))
       {
           $this->courses_id=$data['CourseId'];
       }
        if(array_key_exists('image', $data))
       {
           $this->image=$data['image'];
       }
        return $this;
    }
    
    public function TrainerStore()
    {
        try {
        
            $qery = "INSERT INTO `trainers`(`id`, `unique_id`, `full_name`, `edu_status`, `team`, `courses_id`,
                `trainer_status`, `image`, `phone`, `email`, `address`, `gender`, `web`, `created`, `updated`, `deleted`)
                 VALUES(:id, :uni, :full_name, :edu_status, :team, :courses_id, :trainer_status,:image,:phone, :email, :address,
                 :gender, :web, :created, :updated, :deleted)";
                   $stmt=  $this->connection->prepare($qery);
                   $result=$stmt->execute(array(
                    ':id' => Null,
                    ':uni' => uniqid(),
                   ':full_name' => $this->first_name." ".$this->last_name,
                    ':edu_status' => $this->university." ".$this->lastDegree." ".$this->passingyear." ".$this->eduBoard,
                    ':team' => $this->team,
                   ':courses_id' => $this->courses_id,
                       ':trainer_status' => $this->trainer_status,
                       ':image' => $this->image,
                       ':phone' => $this->phone,
                       ':email' => $this->email,
                       ':address' => $this->presentAddress." ".$this->permanentAddress." ".$this->City." ".$this->zipcode,
                       ':gender' => $this->gender,
                       ':web' => $this->web,
                       ':created' => date("Y-m-d h:i:sA"),
                       ':updated' => date("Y-m-d h:i:sA"),
                       ':deleted' => date("Y-m-d h:i:sA"),
                       ));
                       if ($result)
                       {
                         $_SESSION['error_msg']= '<b style="color: blue;">Information Inserted</b>';
                         unset($_SESSION['AllDAta']);
                         header('location:TrainerAdd.php');
                       }
            
        } catch (Exception $ex) {
            
        }
        
        
    }
    
     public function loginuser()
       {
           try {
               $query= "SELECT * FROM users WHERE username="."'".$this->username."' "."AND password="."'".$this->password."'";
               $stmt=  $this->connection->prepare($query);
               $stmt->execute();
               $userdata=$stmt->fetch();
               echo "<pre>";
//               print_r($userdata);
               
               if($userdata)
               {
                   $_SESSION['username']=  $this->username;
                   $_SESSION['user'] = $userdata;
                  $_SESSION['userid'] = $userdata['id'];
//                  print_r($_SESSION['userid']);
//                  die();
                   $_SESSION['login_msg']='<b style=" color:blue; font-size: 16px; ">Login Successfull <br>Welcome this website';
               } 
               return $userdata;
           } catch (Exception $e) {
               $_SESSION['emty_msg']="request error....!!!";
                header('location:login.php');
           }
       }
       
    public function User_show()
    {
        $qry="SELECT * FROM users WHERE unique_id="."'".$this->unique_id."'";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $row=$stmt->fetch();
       return $row;
    }
       public function ViewAllTrainer()
    {
       $qry="SELECT * FROM trainers";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $table=$stmt->fetchAll();
       return $table;
    }
    
    public function viewAllAssistaint()
    {
        $qry="SELECT * FROM trainers WHERE trainer_status='Assistant Trainer'";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $table=$stmt->fetchAll();
       return $table;
    }

    public function ViewLeadTrainer()
    {
       $qry="SELECT * FROM trainers WHERE trainer_status='Lead Trainer'";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $table=$stmt->fetchAll();
       return $table;
    }
    
        public function logout()
    {
            unset( $_SESSION['emty_msg']);
            unset($_SESSION['user'] );
            unset($_SESSION['login_msg']);
            header('location:login.php');
    }
    
//     $query=  "UPDATE `profiles` SET `users_id` = '100', `first_name` = 'khairull', `last_name` = 'islaml', 
//                   `personal_mobile` = '1784983611', `home_phone` = '1789456894', `address` = 'nothingl', `nationality` = 'bangladeshil',
//                    `Religion` = 'inlaml', `blode_group` = 'B+l', `gender` = 'Malel', `picture` = 'nothingl', 
//                    `dateofbirth` = '09/11/1994l', `nationalID` = 'nothingl', `lastEducationalStatus` = 'csel', `occupation` = 'Employeel', 
//                    `created` = '2:11:44 a', `modified` = '2:11:44 a', `deleted` = '2:11:44 a' WHERE `profiles`.`id` = 1; ";
    
    public function UserUpdate()
    {   
        try {
//              UPDATE `users` SET `full_name` = 'khairul islam a', `email` = 'khairulislamtpi76@gmail.coma', `password` = 'khaiirula', `image` = 'a' WHERE `users`.`id` = 5;


              $qurey = "UPDATE `users` SET `full_name` = '$this->FullName', `email` = '$this->email', `password` = '$this->password', `image` = '$this->image' WHERE `users`.`unique_id`="."'".$this->unique_id."'";
               
              $stmt = $this->connection->prepare($qurey);
                $result=$stmt->execute();
                if($result)
                {
                $_SESSION['update_msg']='<b style=" color: green;">Data Update Successful</b>';
                unset($_SESSION['AllDAta']);
                header("location:Userlist.php");
                }
        } catch (Exception $e) {
            
        }
       
    }
    
    
    public function UserDelete()
    {
        try {
              $query = "DELETE FROM `users` WHERE `users`.`unique_id`="."'".$this->unique_id."'";
//              echo $query;
//              die();
              $stmt = $this->connection->prepare($query);
                if($stmt->execute())
                {
                    $_SESSION['update_msg']='<b style=" color: red;">User Successfully Deleted</b>';
                    header("location:Userlist.php");
                }    
        } catch (Exception $ex) {
            
        }
    }

    

    public function userprofile()
    {
       $query="SELECT * FROM profiles WHERE `profiles`.`users_id` =".$_SESSION['userid'];
       $stmt=  $this->connection->query($query);
       $stmt->execute();
       $fulltable=$stmt->fetch();
       return $fulltable;
    }


}
