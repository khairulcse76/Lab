﻿<?php 
    session_start();
    include 'inc/header.php';
    include 'inc/sidebar.php';
    
        include_once '../vendor/autoload.php';
      
        use ecommerce\brand;
    
        $catlistObj=new brand();
        $data=$catlistObj->brandsview();
        
//        print_r($data);
// cataview()
 
 
 
 
 ?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Brands List</h2>
                <?php if(isset($_SESSION['delete_msg'])) { ?>
                <P>        
                <?php echo $_SESSION['delete_msg']; unset($_SESSION['delete_msg']); ?></P> <?php }?>  
                                            
                <div class="block">        
                         <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Brands Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                                            
                                            <?php 
                                            $id=1;
                                             foreach ($data as $value)
                                             { ?>
                                                
                                            <tr class="odd gradeX">
							<td><?php echo $id++; ?></td>
							<td><?php echo $value['BrandsName']; ?></td>
							<td><a href="brandsedit.php?unique_id=<?php echo $value['unique_id']; ?>">Edit</a> ||
                                                            <a href="bradndelete.php?unique_id=<?php echo $value['unique_id']; ?>">Delete</a></td>
                                            </tr>
                                                
                                            <?php }
                                            ?>
						
						
					</tbody>
                         </table>
               </div>
            </div>
        </div>
<script type="text/javascript">
	$(document).ready(function () {
	    setupLeftMenu();

	    $('.datatable').dataTable();
	    setSidebarHeight();
	});
</script>
<?php include 'inc/footer.php';?>

