<?php
include_once '../vendor/autoload.php';

use labApps\Lab\User\users;

$object=new users();

//print_r($_POST);
//die();
$_SESSION['AllDAta']=$_POST;

$fullname=$_POST['fullName'];
$username=$_POST['userName'];
$email=$_POST['email'];
$password=$_POST['password'];


if(empty($username) || empty($password) || empty($fullname) || empty($email) )
{
     $_SESSION['error_msg']= '<span style="color:red;">(*) Is Required Field</span>';
     header('location:UserEdit.php'); 
} else {
    if(strlen($username)<5 || strlen($username)>12)
    {
       $_SESSION['error_msg']= 'User name must 5 to 12 character';
         header('location:UserEdit.php'); 
    }  else {
       if(strlen($password)<6 || strlen($password)>15)
       {
           $_SESSION['error_msg']= 'Password must 6 to 15 character';
             header('location:UserEdit.php');
       }  else {
           if($_SERVER['REQUEST_METHOD']=='POST')
                {
                  $object->prepare($_POST)->UserUpdate();
                }  else {
                    echo 'Error...404';
                }
              
       }
    }
}
//$object->prepare($_POST)->store();
//echo '<pre>';
//print_r($_POST);